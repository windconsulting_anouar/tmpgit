EXEC = gateway2
CFLAGS = -Wall -c -g
INCLUDEMODBUS   = /usr/include/modbus/
INCLUDEPOSTGRESQL       = /usr/include/postgresql/

CFLAGS += -DDEBUG

CC = gcc ${DEFINE}

OBJECTSgateway = gateway2.o timeZone.o db_database.o tcpModbus.o

clean :
	rm -f  $(EXEC) gateway2.o tcpModbus.o timeZone.o db_database.o db_database.c

all : gateway2

timeZone.o : timeZone.c timeZone.h
	$(CC) $(CFLAGS) -o $@ $<

db_database.c : db_database.pgc
	ecpg -t $<

db_database.o : db_database.c db_database.h
	$(CC) $(CFLAGS) db_database.c -I$(INCLUDEPOSTGRESQL) -o db_database.o

tcpModbus.o : tcpModbus.c tcpModbus.h
	$(CC) $(CFLAGS) tcpModbus.c -I $(INCLUDEMODBUS) -o tcpModbus.o

gateway2.o : timeZone.o db_database.o tcpModbus.o gateway2.c
	$(CC) $(CFLAGS) gateway2.c -o gateway2.o -I $(INCLUDEMODBUS)

gateway2 : ${OBJECTSgateway}
	$(CC) -g ${OBJECTSgateway} -o ${EXEC}  -lpgtypes -lmodbus -lconfig -lpthread -lecpg
