#ifndef __TIMEZONE_H
#define __TIMEZONE_H

#include <time.h>
#include <stdint.h>

char* getUTCTime(time_t rawTime,int16_t * timeZone);
char* getUTCTimeOnly(time_t rawTime);
//#ifdef UNIT_TEST_UTC_TIME
void unitTestGetUTCTime(void);
//#endif
#endif /* __TIMEZONE_H_H */

