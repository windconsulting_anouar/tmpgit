#ifndef TCPMODBUS_H
#define TCPMODBUS_H

#include <modbus.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>

modbus_t * ctx;
const char * ipAddress;
const char * port;
struct timeval response_timeout;

int rc;


void modbusTcp_setDebug(uint8_t debug);
void modbusTcp_setIpAddress(const char * ip, const char * port);
void modbusTcp_setResponseTimeout( uint32_t sec, uint32_t usec);
int modbusTcp_connect(void);
void modbusTcp_close(void);
void modbusTcp_cleanup(void);


int modbusTcp_readInputRegisters(uint16_t slave, uint16_t regAddress, uint16_t regsNbr, uint16_t * buffer);
int modbusTcp_readHoldingRegisters(uint16_t slave, uint16_t regAddress, uint16_t regsNbr, uint16_t * buffer);
// int modbusTcp_readInputsStatus(uint16_t slave, uint16_t coilAdress, uint16_t inputNbr, uint8_t * values);
// int modbusTcp_readCoilStatus(uint16_t slave, uint16_t coilAdress, uint16_t coilsNbr, uint8_t * values);

// int modbusTcp_writeRegister(uint16_t slave, uint16_t regAddress, uint16_t value);
// int modbusTcp_writeRegisters(uint16_t slave, uint16_t regAddress, uint16_t regsNbr, uint16_t * buffer);
// int modbusTcp_writeBitStatus(uint16_t slave, uint16_t coilAdress, uint8_t status);
// int modbusTcp_writeBitsStatus(uint16_t slave, uint16_t coilAdress, uint16_t coilsNbr, u_int8_t * buffer);

#endif // TCPMODBUS_H










