
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libconfig.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include "tcpModbus.h"
#include "db_database.h"

#define 	POW_2_0			0x0000000000001
#define 	POW_2_16		0x0000000010000
#define 	POW_2_32		0x0000100000000
#define 	POW_2_48		0x1000000000000

#define CONFIG_FILENAME 	"gateway2.cfg"
#define BUFF_SIZE       	128
#define BUFF_FILE_SIZE    	10000
#define MEAS_EGX_FILE_PATH	"/home/sems/measurementsEGX001"

typedef struct {
	char host[20];
	char port[10];
	char name[50];
	char username[50];
	char password[50];
} dbinfo_t;

typedef enum  {
	NSX150		= 0x01,
	NSX300 		= 0x02,
	IEM 		= 0x03,
	MT_P 		= 0x04,
	A9MEM15xx	= 0x05
} meterType;

static dbinfo_t db;

static uint16_t bufferRead[BUFF_SIZE];
static char bufferFile[BUFF_FILE_SIZE];
static uint16_t tmpBuff[4];
static FILE *storeDataFile = NULL;
uint8_t slaveIdx;
int terminalId;
uint32_t mesurementTime;
uint32_t ipUint_32;
struct timeval begin_measure, end_maesure;
struct tm * current_time;
uint8_t mes_success[32];
int i;


#define IP_01		"10.170.80.145"
#define IP_02		"10.170.80.202"
#define IP_03		"10.170.81.78"
#define IP_04		"10.170.81.136"
#define IP_05		"10.170.81.200"
#define IP_06		"10.170.82.70"
///////////////////////////
#define IP_07		"10.170.82.78"
#define IP_08		"10.170.80.74"
#define IP_09		"10.170.80.201"
#define IP_10		"10.170.80.203"
///////////////////
#define PORT		"502"
#define MAX_TRY_NBR  3


const uint8_t slaveArrNSX_01[] = {1, 2, 3, 4};
const uint8_t slaveArrNSX_02[] = {1, 2, 3, 4, 5, 6, 7, 8};
const uint8_t slaveArrNSX_03[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
const uint8_t slaveArrNSX_04[] = {1, 2, 3, 4, 5, 6};
const uint8_t slaveArrNSX_05[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
const uint8_t slaveArrNSX_06[] = {5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};

const uint8_t slaveArrMT_P_06[] = {1, 2};

const uint8_t slaveArrIEM3150_06[] = {21, 22, 23, 24, 25, 26, 27, 28};

const uint8_t slaveArrA9MEM15xx_07[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
const uint8_t slaveArrA9MEM15xx_08[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
const uint8_t slaveArrA9MEM15xx_09[] = {1, 2, 3, 4, 5, 6, 7, 8};
const uint8_t slaveArrA9MEM15xx_10[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t cond = PTHREAD_COND_INITIALIZER;


int readConfig();
void * threadMesure(void *argument);

double timeExection(struct timeval * tv1, struct timeval * tv2);
float decryptToFloat(uint16_t * buffPtr);
int64_t decryptToInt64(uint16_t * buffPtr);
int32_t decrypt_NSX_EGX300ToInt32(uint16_t * buffPtr);
int64_t decryptToNsxInt32(uint16_t * buffPtr);
int64_t decrypt_MT_P_ToInt64(uint16_t * buffPtr);

void read_NSX_EGX150 (char * ip, char * port, const uint8_t * slaveArr, uint8_t slaveNbr, uint8_t tryNbr);
void read_NSX_EGX300 (char * ip, char * port, const uint8_t * slaveArr, uint8_t slaveNbr, uint8_t tryNbr);
void read_IEM3150 (char * ip, char * port, const uint8_t * slaveArr, uint8_t slaveNbr, uint8_t tryNbr);
void read_MT_P(char * ip, char * port, const uint8_t * slaveArr, uint8_t slaveNbr, uint8_t tryNbr);
void read_A9MEM15xx(char * ip, char * port, const uint8_t * slaveArr, uint8_t slaveNbr, uint8_t tryNbr);


FILE* createNewFile(const char* SEMSEmsMesFolder);
void flushMeasToFile();
void writeMeasToCsv(uint16_t * bufferRead, int terminalID, int datetime, uint8_t meterType);

int main(int argc, char **argv)
{
	/* read from config file */
	if(readConfig() == -1){
		printf("\r\nError reading config file!\r\n");
		exit(EXIT_FAILURE);
	}


		modbusTcp_setResponseTimeout(0, 500000);

		/* read all NSX EGX150 */
		read_NSX_EGX150(IP_01, PORT, slaveArrNSX_01, sizeof(slaveArrNSX_01) / sizeof(uint8_t), MAX_TRY_NBR);
		read_NSX_EGX150(IP_02, PORT, slaveArrNSX_02, sizeof(slaveArrNSX_02) / sizeof(uint8_t), MAX_TRY_NBR);
		read_NSX_EGX150(IP_03, PORT, slaveArrNSX_03, sizeof(slaveArrNSX_03) / sizeof(uint8_t), MAX_TRY_NBR);
		read_NSX_EGX150(IP_04, PORT, slaveArrNSX_04, sizeof(slaveArrNSX_04) / sizeof(uint8_t), MAX_TRY_NBR);

		/* read all NSX EGX150 */
		read_NSX_EGX300(IP_05, PORT, slaveArrNSX_05, sizeof(slaveArrNSX_05) / sizeof(uint8_t), MAX_TRY_NBR);  // use MT_P registers for this NSX
		read_NSX_EGX300(IP_06, PORT, slaveArrNSX_06, sizeof(slaveArrNSX_06) / sizeof(uint8_t), MAX_TRY_NBR);  // use MT_P registers for this NSX

		/* read all MT_P */
		read_MT_P(IP_06, PORT, slaveArrMT_P_06, sizeof(slaveArrMT_P_06) / sizeof(uint8_t), MAX_TRY_NBR);

		/* read all IEM3150 */
		read_IEM3150(IP_06, PORT, slaveArrIEM3150_06, sizeof(slaveArrIEM3150_06) / sizeof(uint8_t), MAX_TRY_NBR);

		/* read all A9MEM15xx */
		read_A9MEM15xx(IP_07, PORT, slaveArrA9MEM15xx_07, sizeof(slaveArrA9MEM15xx_07) / sizeof(uint8_t), MAX_TRY_NBR);
		read_A9MEM15xx(IP_08, PORT, slaveArrA9MEM15xx_08, sizeof(slaveArrA9MEM15xx_08) / sizeof(uint8_t), MAX_TRY_NBR);
		read_A9MEM15xx(IP_09, PORT, slaveArrA9MEM15xx_09, sizeof(slaveArrA9MEM15xx_09) / sizeof(uint8_t), MAX_TRY_NBR);
		read_A9MEM15xx(IP_10, PORT, slaveArrA9MEM15xx_10, sizeof(slaveArrA9MEM15xx_10) / sizeof(uint8_t), MAX_TRY_NBR);

		storeDataFile = createNewFile(MEAS_EGX_FILE_PATH);
		flushMeasToFile(storeDataFile);
		pclose(storeDataFile);

		printf("========================================================================================================================================================\n\n\n");
	return 0;
} // end of main




int readConfig()
{
	config_t cfg;
	const char * l_str = NULL;
	config_init(&cfg);

	printf("\r\n========================== READING FROM CONFIG FILE ==========================\r\n");

	if (!config_read_file(&cfg, CONFIG_FILENAME)) {
		printf("Configuration file not found!");
		goto error;
	}

	/* read database host */
	if (!config_lookup_string(&cfg, "DB_HOST", &l_str)) {
		printf("No 'database host' setting in configuration file.\r\n");
		goto error;
	} else {
		strcpy(db.host, l_str);
		printf("\r\n[DB_HOST] : %s\r\n", db.host);
	}

	/* read database port */
	if (!config_lookup_string(&cfg, "DB_PORT", &l_str)) {
		printf("No 'database port' setting in configuration file.\r\n");
		goto error;
	} else {
		strcpy(db.port, l_str);
		printf("[DB_PORT] : %s\r\n", db.port);
	}

	/* read database name */
	if (!config_lookup_string(&cfg, "DB_NAME", &l_str)) {
		printf("No 'database name' setting in configuration file.\r\n");
		goto error;
	} else {
		strcpy(db.name, l_str);
		printf("[DB_NAME] : %s\r\n", db.name);
	}

	/* read database sems */
	if (!config_lookup_string(&cfg, "DB_USERNAME", &l_str)) {
		printf("No 'database username' setting in configuration file.\r\n");
		goto error;
	} else {
		strcpy(db.username, l_str);
		printf("[DB_USERNAME] : %s\r\n", db.username);
	}

	/* read database password */
	if (!config_lookup_string(&cfg, "DB_PASSWORD", &l_str)) {
		printf("No 'database password' setting in configuration file.\r\n");
		goto error;
	} else {
		strcpy(db.password, l_str);
		printf("[DB_PASSWORD] : %s\r\n", db.password);
	}

	printf("==============================================================================\r\n");
	return 0; // read config success

	error:
		fflush(stdout);
		config_destroy(&cfg);
		return -1;
} // end of readConfig


double timeExection(struct timeval * tv1, struct timeval * tv2)
{
	return (double)(tv2->tv_usec - tv1->tv_usec) / 1000000 + (double)(tv2->tv_sec - tv1->tv_sec);
} // end of timeExection


inline float decryptToFloat(uint16_t * buffPtr)
{
	tmpBuff[0] = buffPtr[1];
	tmpBuff[1] = buffPtr[0];
	return *(float*)(tmpBuff);
} // end of decryptToFloat

inline int64_t decryptToInt64(uint16_t * buffPtr)
{
	return (int64_t)(buffPtr[3] | buffPtr[2] << 8 | buffPtr[1] << 16 | buffPtr[0] << 24);
} // end of decryptToInt64


inline int32_t decrypt_NSX_EGX300ToInt32(uint16_t * buffPtr)
{
	tmpBuff[0] = buffPtr[1];
	tmpBuff[1] = buffPtr[0];
	return *(int32_t*)tmpBuff;
} // end of decrypt_NSX_EGX300ToInt32

inline int64_t decrypt_MT_P_ToInt64(uint16_t * buffPtr)
{
	return (int64_t)buffPtr[0] + (int64_t)buffPtr[1] * 10000 + (int64_t)buffPtr[2] * 100000000 + (int64_t)buffPtr[3] * 1000000000000;
} // end of decrypt_MT_P_ToInt64

inline int64_t decryptToNsxInt32(uint16_t * buffPtr)
{
	return (buffPtr[3] * POW_2_0 + buffPtr[2] * POW_2_16  + buffPtr[1] * POW_2_32  + buffPtr[0] * POW_2_48);
} // end of decryptToNsxInt32

void read_NSX_EGX150 (char * ip, char * port, const uint8_t * slaveArr, uint8_t slaveNbr, uint8_t tryNbr)
{
	modbusTcp_setIpAddress(ip, port);
	ipUint_32 = inet_addr(ip);

	printf("############################################################# %s #############################################################\n",ip);

	for (i = 0; i < slaveNbr; ++i) {
		mes_success[i] = 0;
	}

	mesurementTime = time(NULL);

	for(i = 0; i < tryNbr; ++i)
	{
		for (slaveIdx = 0; slaveIdx < slaveNbr; ++slaveIdx)
		{
			if(mes_success[slaveIdx] == 0 ){
				gettimeofday(&begin_measure, NULL);
				terminalId = slaveArr[slaveIdx] + (1000 * ((ipUint_32 & 0xFF000000) >> 24)) + (1000000 * ((ipUint_32 & 0x00FF0000) >> 16));

				/* activeEnergy, reactiveEnergy, activePower reactivePower */
				if(modbusTcp_readInputRegisters(slaveArr[slaveIdx], 32077, 26, bufferRead) == 0) {
					/* voltage, voltage1, voltage2, voltage3, current, current1, current2, current3 */
					if(modbusTcp_readInputRegisters(slaveArr[slaveIdx], 1002, 25, bufferRead + 26) == 0) {
						gettimeofday(&end_maesure, NULL);
						mes_success[slaveIdx] = 1;
#ifdef DEBUG
						printf("------------------------------------------------------------------ %d -------------------------------------------------------------------\n", slaveArr[slaveIdx]);
						printf("I: %.2f (A)\tI1: %.2f (A)\tI2: %.2f (A)\tI3: %.2f\t (A)V: %.2f (V)\tV1: %.2f\tV2: %.2f\t (V)V3: %.2f (V)\n"
								"PA: %.2f (Kw)\tPR: %.2f (KVar)\tEA: %.2f (KwH)\tER: %.2f (KVarH)\t  [%f sec]\n",
								(float)*(bufferRead + 50),								/* current */
								(float)*(bufferRead + 39),								/* current1 */
								(float)*(bufferRead + 40),								/* current2 */
								(float)*(bufferRead + 41),								/* current3 */
								(float)*(bufferRead + 29),								/* voltage */
								(float)*(bufferRead + 26),								/* voltage1 */
								(float)*(bufferRead + 27),								/* voltage2 */
								(float)*(bufferRead + 28),								/* voltage3 */
								decryptToFloat(bufferRead + 0) / 1000.0,				/* activePower */
								decryptToFloat(bufferRead + 8) / 1000.0,		 		/* reactivePower */
								(float)decryptToNsxInt32(bufferRead + 18) / 1000.0,	 	/* activeEnergy */
								(float)decryptToNsxInt32(bufferRead + 22) / 1000.0, 		/* reactiveEnergy */
								timeExection(&begin_measure, &end_maesure) 				/* exec time */
						);
#endif
						// /* insert to local DB*/
						// if (!databaseConnect(db.host, db.port, db.name, db.username, db.password)) /* connect to database */
						// 	printf("[localDB]: database connection failed\r\n");
						// else {
						// 	if (!databaseInsertStandard2(terminalId, mesurementTime,
						// 													(float)*(bufferRead + 50),								/* current */
						// 													(float)*(bufferRead + 39),								/* current1 */
						// 													(float)*(bufferRead + 40),								/* current2 */
						// 													(float)*(bufferRead + 41),								/* current3 */
						// 													(float)*(bufferRead + 29),								/* voltage */
						// 													(float)*(bufferRead + 26),								/* voltage1 */
						// 													(float)*(bufferRead + 27),								/* voltage2 */
						// 													(float)*(bufferRead + 28),								/* voltage3 */
						// 													(float)decryptToNsxInt32(bufferRead + 18) / 1000.0,	 	/* activeEnergy */
						// 													(float)decryptToNsxInt32(bufferRead + 22) / 1000.0, 	/* reactiveEnergy */
						// 													decryptToFloat(bufferRead + 0) / 1000.0,				/* activePower */
						// 													decryptToFloat(bufferRead + 8) / 1000.0,		 		/* reactivePower */
						// 													0)) {printf("insertion failed to local DB\r\n");}
						// 	/* close database connection */
						// 	databaseDisconnect();
						// }

						/* write to CSV file */
						writeMeasToCsv(bufferRead, terminalId, mesurementTime, NSX150);
					}
				}
			}
			fflush(stdout);
		}
	}

	for (slaveIdx = 0; slaveIdx < slaveNbr; ++slaveIdx)
	{
		if (mes_success[slaveIdx] == 0)
			printf("Error read slave %d\n", slaveArr[slaveIdx]);
	}
} // end of read_NSX


void read_NSX_EGX300 (char * ip, char * port, const uint8_t * slaveArr, uint8_t slaveNbr, uint8_t tryNbr)
{
	modbusTcp_setIpAddress(ip, port);
	ipUint_32 = inet_addr(ip);

	printf("############################################################# %s #############################################################\n",ip);

	for (i = 0; i < slaveNbr; ++i) {
		mes_success[i] = 0;
	}

	mesurementTime = time(NULL);

	for(i = 0; i < tryNbr; ++i)
	{
		for (slaveIdx = 0; slaveIdx < slaveNbr; ++slaveIdx)
		{
			if(mes_success[slaveIdx] == 0 ){
				gettimeofday(&begin_measure, NULL);
				/* activeEnergy, reactiveEnergy */
				if(modbusTcp_readHoldingRegisters(slaveArr[slaveIdx], 1999, 8, bufferRead) == 0) {
					/* voltage, voltage1, voltage2, voltage3, current, current1, current2, current3 activePower reactivePower */
					if(modbusTcp_readHoldingRegisters(slaveArr[slaveIdx], 1002, 40, bufferRead + 8) == 0) {
						gettimeofday(&end_maesure, NULL);
						mes_success[slaveIdx] = 1;
						terminalId = slaveArr[slaveIdx] + (1000 * ((ipUint_32 & 0xFF000000) >> 24)) + (1000000 * ((ipUint_32 & 0x00FF0000) >> 16));
#ifdef DEBUG
						printf("--------------------------------- %d ----------------------------------\n", slaveArr[slaveIdx]);
						printf("I: %.2f (A)\tI1: %.2f (A)\tI2: %.2f (A)\tI3: %.2f\t (A)V: %.2f (V)\tV1: %.2f\tV2: %.2f\t (V)V3: %.2f (V)\n"
								"PA: %.2f (Kw)\tPR: %.2f (KVar)\tEA: %.2f (KwH)\tER: %.2f (KVarH)\t  [%f sec]\n",
								(float)*(bufferRead + 32),						/* current */
								(float)*(bufferRead + 21),						/* current1 */
								(float)*(bufferRead + 22),						/* current2 */
								(float)*(bufferRead + 23),						/* current3 */
								(float)*(bufferRead + 11),						/* voltage */
								(float)*(bufferRead + 8),						/* voltage1 */
								(float)*(bufferRead + 9),						/* voltage2 */
								(float)*(bufferRead + 10),						/* voltage3 */
								(float)*(bufferRead + 42),						/* activePower */
								(float)*(bufferRead + 44),		 				/* reactivePower */
								(float)decrypt_NSX_EGX300ToInt32(bufferRead),	 	/* activeEnergy */
								(float)decrypt_NSX_EGX300ToInt32(bufferRead + 4), 	/* reactiveEnergy */
								timeExection(&begin_measure, &end_maesure) 		/* exec time */
						);
#endif

						// /* connect to database */
						// if (!databaseConnect(db.host, db.port, db.name, db.username, db.password))
						// 	printf("database connection failed\r\n");
						// else {
						// 	if (!databaseInsertStandard2(terminalId, mesurementTime,
						// 													(float)*(bufferRead + 32),						/* current */
						// 													(float)*(bufferRead + 21),						/* current1 */
						// 													(float)*(bufferRead + 22),						/* current2 */
						// 													(float)*(bufferRead + 23),						/* current3 */
						// 													(float)*(bufferRead + 11),						/* voltage */
						// 													(float)*(bufferRead + 8),						/* voltage1 */
						// 													(float)*(bufferRead + 9),						/* voltage2 */
						// 													(float)*(bufferRead + 10),						/* voltage3 */
						// 													(float)decrypt_NSX_EGX300ToInt32(bufferRead),	 	/* activeEnergy */
						// 													(float)decrypt_NSX_EGX300ToInt32(bufferRead + 4), 	/* reactiveEnergy */
						// 													(float)*(bufferRead + 42),						/* activePower */
						// 													(float)*(bufferRead + 44),		 				/* reactivePower */
						// 													0)) {printf("insertion failed\r\n");}
						// 	/* close database connection */
						// 	databaseDisconnect();
						// }

						/* write to CSV file */
						writeMeasToCsv(bufferRead, terminalId, mesurementTime, NSX300);
					}
				}
			}
			fflush(stdout);
		}
	}


	for (slaveIdx = 0; slaveIdx < slaveNbr; ++slaveIdx)
	{
		if (mes_success[slaveIdx] == 0)
			printf("Error read slave %d\n", slaveArr[slaveIdx]);
	}
} // end of read_NSX_EGX300


void read_MT_P (char * ip, char * port, const uint8_t * slaveArr, uint8_t slaveNbr, uint8_t tryNbr)
{
	modbusTcp_setIpAddress(ip, port);
	ipUint_32 = inet_addr(ip);

	printf("############################################################# %s #############################################################\n",ip);

	for (i = 0; i < slaveNbr; ++i) {
		mes_success[i] = 0;
	}

	mesurementTime = time(NULL);

	for(i = 0; i < tryNbr; ++i)
	{
		for (slaveIdx = 0; slaveIdx < slaveNbr; ++slaveIdx)
		{
			if(mes_success[slaveIdx] == 0 ){
				gettimeofday(&begin_measure, NULL);
				/* activeEnergy, reactiveEnergy */
				if(modbusTcp_readHoldingRegisters(slaveArr[slaveIdx], 1999, 8, bufferRead) == 0) {
					/* voltage, voltage1, voltage2, voltage3, current, current1, current2, current3 activePower reactivePower */
					if(modbusTcp_readHoldingRegisters(slaveArr[slaveIdx], 1002, 40, bufferRead + 8) == 0) {
						gettimeofday(&end_maesure, NULL);
						mes_success[slaveIdx] = 1;
						terminalId = slaveArr[slaveIdx] + (1000 * ((ipUint_32 & 0xFF000000) >> 24)) + (1000000 * ((ipUint_32 & 0x00FF0000) >> 16));
#ifdef DEBUG
						printf("--------------------------------- %d ----------------------------------\n", slaveArr[slaveIdx]);
						printf("I: %.2f (A)\tI1: %.2f (A)\tI2: %.2f (A)\tI3: %.2f\t (A)V: %.2f (V)\tV1: %.2f\tV2: %.2f\t (V)V3: %.2f (V)\n"
								"PA: %.2f (Kw)\tPR: %.2f (KVar)\tEA: %.2f (KwH)\tER: %.2f (KVarH)\t  [%f sec]\n",
								(float)*(bufferRead + 32),						/* current */
								(float)*(bufferRead + 21),						/* current1 */
								(float)*(bufferRead + 22),						/* current2 */
								(float)*(bufferRead + 23),						/* current3 */
								(float)*(bufferRead + 11),						/* voltage */
								(float)*(bufferRead + 8),						/* voltage1 */
								(float)*(bufferRead + 9),						/* voltage2 */
								(float)*(bufferRead + 10),						/* voltage3 */
								(float)*(bufferRead + 42),						/* activePower */
								(float)*(bufferRead + 44),		 				/* reactivePower */
								(float)decrypt_MT_P_ToInt64(bufferRead),	 	/* activeEnergy */
								(float)decrypt_MT_P_ToInt64(bufferRead + 4), 	/* reactiveEnergy */
								timeExection(&begin_measure, &end_maesure) 		/* exec time */
						);
#endif

						// /* connect to database */
						// if (!databaseConnect(db.host, db.port, db.name, db.username, db.password))
						// 	printf("database connection failed\r\n");
						// else {
						// 	if (!databaseInsertStandard2(terminalId, mesurementTime,
						// 													(float)*(bufferRead + 32),						/* current */
						// 													(float)*(bufferRead + 21),						/* current1 */
						// 													(float)*(bufferRead + 22),						/* current2 */
						// 													(float)*(bufferRead + 23),						/* current3 */
						// 													(float)*(bufferRead + 11),						/* voltage */
						// 													(float)*(bufferRead + 8),						/* voltage1 */
						// 													(float)*(bufferRead + 9),						/* voltage2 */
						// 													(float)*(bufferRead + 10),						/* voltage3 */
						// 													(float)decrypt_NSX_EGX300ToInt32(bufferRead),	 	/* activeEnergy */
						// 													(float)decrypt_NSX_EGX300ToInt32(bufferRead + 4), 	/* reactiveEnergy */
						// 													(float)*(bufferRead + 42),						/* activePower */
						// 													(float)*(bufferRead + 44),		 				/* reactivePower */
						// 													0)) {printf("insertion failed\r\n");}
						// 	/* close database connection */
						// 	databaseDisconnect();
						// }

						/* write to CSV file */
						writeMeasToCsv(bufferRead, terminalId, mesurementTime, MT_P);
					}
				}
			}
			fflush(stdout);
		}
	}


	for (slaveIdx = 0; slaveIdx < slaveNbr; ++slaveIdx)
	{
		if (mes_success[slaveIdx] == 0)
			printf("Error read slave %d\n", slaveArr[slaveIdx]);
	}
} // end of read_MT_P


void read_IEM3150 (char * ip, char * port, const uint8_t * slaveArr, uint8_t slaveNbr, uint8_t tryNbr)
{
	modbusTcp_setIpAddress(ip, port);
	ipUint_32 = inet_addr(ip);

	printf("############################################################# %s #############################################################\n",ip);

	for (i = 0; i < slaveNbr; ++i) {
		mes_success[i] = 0;
	}

	mesurementTime = time(NULL);

	for(i = 0; i < tryNbr; ++i)
	{
		for (slaveIdx = 0; slaveIdx < slaveNbr; ++slaveIdx)
		{
			if(mes_success[slaveIdx] == 0 ){
				gettimeofday(&begin_measure, NULL);
				/* voltage, voltage1, voltage2, voltage3, current, current1, current2, current3 activePower*/
				if(modbusTcp_readHoldingRegisters(slaveArr[slaveIdx], 2999, 62, bufferRead) == 0) {
					/* active energy */
					if(modbusTcp_readHoldingRegisters(slaveArr[slaveIdx], 3203, 4, bufferRead + 62) == 0) {
						gettimeofday(&end_maesure, NULL);
						mes_success[slaveIdx] = 1;
						terminalId = slaveArr[slaveIdx] + (1000 * ((ipUint_32 & 0xFF000000) >> 24)) + (1000000 * ((ipUint_32 & 0x00FF0000) >> 16));
#ifdef DEBUG
						printf("--------------------------------- %d ----------------------------------\n", slaveArr[slaveIdx]);
						printf("I: %.2f (A)\tI1: %.2f (A)\tI2: %.2f (A)\tI3: %.2f\t (A)V: %.2f (V)\tV1: %.2f\tV2: %.2f\t (V)V3: %.2f (V)\nPA: %.2f (Kw)\tEA: %.2f (KwH)\t  [%f sec]\n",
								decryptToFloat(bufferRead + 10),				/* current */
								decryptToFloat(bufferRead),						/* current1 */
								decryptToFloat(bufferRead + 2),					/* current2 */
								decryptToFloat(bufferRead + 4),					/* current3 */
								decryptToFloat(bufferRead + 26),				/* voltage */
								decryptToFloat(bufferRead + 28),				/* voltage1 */
								decryptToFloat(bufferRead + 30),				/* voltage2 */
								decryptToFloat(bufferRead + 32),				/* voltage3 */
								decryptToFloat(bufferRead + 60),				/* activePower */
								(float)decryptToInt64(bufferRead + 62),			/* activeEnergy */
								timeExection(&begin_measure, &end_maesure)		/* exec time */
						);
#endif
						// /* connect to database */
						// if (!databaseConnect(db.host, db.port, db.name, db.username, db.password))
						// 	printf("database connection failed\r\n");
						// else {
						// 	if (!databaseInsertStandard2(terminalId, mesurementTime,
						// 													decryptToFloat(bufferRead + 10),				/* current */
						// 													decryptToFloat(bufferRead),						/* current1 */
						// 													decryptToFloat(bufferRead + 2),					/* current2 */
						// 													decryptToFloat(bufferRead + 4),					/* current3 */
						// 													decryptToFloat(bufferRead + 26),				/* voltage */
						// 													decryptToFloat(bufferRead + 28),				/* voltage1 */
						// 													decryptToFloat(bufferRead + 30),				/* voltage2 */
						// 													decryptToFloat(bufferRead + 32),				/* voltage3 */
						// 													(float)decryptToInt64(bufferRead + 62),			/* activeEnergy */
						// 													NAN, 											/* reactiveEnergy */
						// 													decryptToFloat(bufferRead + 60),				/* activePower */
						// 													NAN,		 									/* reactivePower */
						// 													0)) {printf("insertion failed\r\n");}
						// 	/* close database connection */
						// 	databaseDisconnect();
						// }

						/* write to CSV file */
						writeMeasToCsv(bufferRead, terminalId, mesurementTime, IEM);
					}
				}
			}
			fflush(stdout);
		}
	}

	for (slaveIdx = 0; slaveIdx < slaveNbr; ++slaveIdx)
	{
		if (mes_success[slaveIdx] == 0)
			printf("Error read slave %d\n", slaveArr[slaveIdx]);
	}
} // end of  read_IEM3150

void read_A9MEM15xx(char * ip, char * port, const uint8_t * slaveArr, uint8_t slaveNbr, uint8_t tryNbr)
{
	modbusTcp_setIpAddress(ip, port);
	ipUint_32 = inet_addr(ip);

	printf("############################################################# %s #############################################################\n",ip);

	for (i = 0; i < slaveNbr; ++i) {
		mes_success[i] = 0;
	}

	mesurementTime = time(NULL);

	for(i = 0; i < tryNbr; ++i)
	{
		for (slaveIdx = 0; slaveIdx < slaveNbr; ++slaveIdx)
		{
			if(mes_success[slaveIdx] == 0 ){
				gettimeofday(&begin_measure, NULL);
				/* current1, current2, current3 voltage, voltage1, voltage2, voltage3 */
 				if(modbusTcp_readHoldingRegisters(slaveArr[slaveIdx], 2999, 34, bufferRead) == 0) {
					/* active power, reactive power */
					if(modbusTcp_readHoldingRegisters(slaveArr[slaveIdx], 3259, 10, bufferRead + 34) == 0) {
						/* active energy*/
						if(modbusTcp_readHoldingRegisters(slaveArr[slaveIdx], 3211, 4, bufferRead + 44) == 0) {
							/* reactive energy */
							if(modbusTcp_readHoldingRegisters(slaveArr[slaveIdx], 3279, 4, bufferRead + 48) == 0) {
								gettimeofday(&end_maesure, NULL);
								mes_success[slaveIdx] = 1;
								terminalId = slaveArr[slaveIdx] + (1000 * ((ipUint_32 & 0xFF000000) >> 24)) + (1000000 * ((ipUint_32 & 0x00FF0000) >> 16));
#ifdef DEBUG
								printf("--------------------------------- %d ----------------------------------\n", slaveArr[slaveIdx]);
								printf("I: %.2f (A)\tI1: %.2f (A)\tI2: %.2f (A)\tI3: %.2f\t (A)V: %.2f (V)\tV1: %.2f\tV2: %.2f\t (V)V3: %.2f (V)\nPA: %.2f (Kw)\tEA: %.2f (KwH)\t  [%f sec]\n",
										NAN,				/* current */
										decryptToFloat(bufferRead),							/* current1 */
										decryptToFloat(bufferRead + 2),						/* current2 */
										decryptToFloat(bufferRead + 4),						/* current3 */
										decryptToFloat(bufferRead + 20),					/* voltage */
										decryptToFloat(bufferRead + 28),					/* voltage1 */
										decryptToFloat(bufferRead + 30),					/* voltage2 */
										decryptToFloat(bufferRead + 32),					/* voltage3 */
										decryptToFloat(bufferRead + 34) / 1000.0,			/* activePower */
										decryptToFloat(bufferRead + 42) / 1000.0,			/* reactivePower */
										(float)decryptToInt64(bufferRead + 44) / 1000.0,	/* activeEnergy */
										(float)decryptToInt64(bufferRead + 48) / 1000.0,	/* reactiveEnergy */
										timeExection(&begin_measure, &end_maesure)			/* exec time */
								);
#endif
								// /* connect to database */
								// if (!databaseConnect(db.host, db.port, db.name, db.username, db.password))
								// 	printf("database connection failed\r\n");
								// else {
								// 	if (!databaseInsertStandard2(terminalId, mesurementTime,
								// 													NAN,											/* current */
								// 													decryptToFloat(bufferRead),						/* current1 */
								// 													decryptToFloat(bufferRead + 2),					/* current2 */
								// 													decryptToFloat(bufferRead + 4),					/* current3 */
								// 													decryptToFloat(bufferRead + 20),				/* voltage */
								// 													decryptToFloat(bufferRead + 28),				/* voltage1 */
								// 													decryptToFloat(bufferRead + 30),				/* voltage2 */
								// 													decryptToFloat(bufferRead + 32),				/* voltage3 */
								// 													decryptToFloat(bufferRead + 34),				/* activePower */
								// 													decryptToFloat(bufferRead + 42),		 		/* reactivePower */
								// 													(float)decryptToInt64(bufferRead + 44),			/* activeEnergy */
								// 													(float)decryptToInt64(bufferRead + 48), 											/* reactiveEnergy */
								// 													0)) {printf("insertion failed\r\n");}
								// 	/* close database connection */
								// 	databaseDisconnect();
								// }

								/* write to CSV file */
								writeMeasToCsv(bufferRead, terminalId, mesurementTime, A9MEM15xx);
							}
						}
					}
				}

			}
			fflush(stdout);
		}
	}

	for (slaveIdx = 0; slaveIdx < slaveNbr; ++slaveIdx)
	{
		if (mes_success[slaveIdx] == 0)
			printf("Error read slave %d\n", slaveArr[slaveIdx]);
	}
} // end of  read_A9MEM15xx

FILE* createNewFile(const char* SEMSEmsMesFolder)
{
	struct stat sterr = {0};
	if (stat(SEMSEmsMesFolder, &sterr) == -1) {
		mkdir(SEMSEmsMesFolder, 0700);
	}
	char szFileName[255] = {0};
	sprintf(szFileName, "%s/measurements.txt",SEMSEmsMesFolder);
	return fopen(szFileName,"w");
} // end of createNewFile

void flushMeasToFile(FILE * file)
{
	fwrite(bufferFile, strlen(bufferFile), sizeof(char), file);
	fflush(storeDataFile);
	memset(bufferFile, 0x00, BUFF_FILE_SIZE);
}

void writeMeasToCsv(uint16_t * bufferRead, int terminalID, int datetime, uint8_t meterType)
{
	// static char buffRow[300];
	// memset(buffRow, 0x00, 300);

	// switch (meterType) {
	// case NSX150:
	// 	sprintf(buffRow, "%d;%d;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f\n",
	// 									terminalID,
	// 									mesurementTime,
	// 									(float)*(bufferRead + 50),								/* current */
	// 									(float)*(bufferRead + 39),								/* current1 */
	// 									(float)*(bufferRead + 40),								/* current2 */
	// 									(float)*(bufferRead + 41),								/* current3 */
	// 									(float)*(bufferRead + 26),								/* voltage1 */
	// 									(float)*(bufferRead + 27),								/* voltage2 */
	// 									(float)*(bufferRead + 28),								/* voltage3 */
	// 									(float)*(bufferRead + 29),								/* voltage */
	// 									(float)decryptToNsxInt32(bufferRead + 18) / 1000.0,	 	/* activeEnergy */
	// 									(float)decryptToNsxInt32(bufferRead + 22) / 1000.0, 		/* reactiveEnergy */
	// 									decryptToFloat(bufferRead + 0) / 1000.0,				/* activePower */
	// 									decryptToFloat(bufferRead + 8) / 1000.0				/* reactivePower */
	// 	);
	// 	break;
	// case NSX300:
	// 	sprintf(buffRow, "%d;%d;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f\n",
	// 									terminalID,
	// 									mesurementTime,
	// 									(float)*(bufferRead + 32),						/* current */
	// 									(float)*(bufferRead + 21),						/* current1 */
	// 									(float)*(bufferRead + 22),						/* current2 */
	// 									(float)*(bufferRead + 23),						/* current3 */
	// 									(float)*(bufferRead + 8),						/* voltage1 */
	// 									(float)*(bufferRead + 9),						/* voltage2 */
	// 									(float)*(bufferRead + 10),						/* voltage3 */
	// 									(float)*(bufferRead + 11),						/* voltage */
	// 									(float)decrypt_NSX_EGX300ToInt32(bufferRead),	 	/* activeEnergy */
	// 									(float)decrypt_NSX_EGX300ToInt32(bufferRead + 4), 	/* reactiveEnergy */
	// 									(float)*(bufferRead + 42),						/* activePower */
	// 									(float)*(bufferRead + 44)		 				/* reactivePower */
	// 	);
	// 	break;
	// case IEM:
	// 	sprintf(buffRow, "%d;%d;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f\n",
	// 									terminalID,
	// 									mesurementTime,
	// 									decryptToFloat(bufferRead + 10),				/* current */
	// 									decryptToFloat(bufferRead),						/* current1 */
	// 									decryptToFloat(bufferRead + 2),					/* current2 */
	// 									decryptToFloat(bufferRead + 4),					/* current3 */
	// 									decryptToFloat(bufferRead + 28),				/* voltage1 */
	// 									decryptToFloat(bufferRead + 30),				/* voltage2 */
	// 									decryptToFloat(bufferRead + 32),				/* voltage3 */
	// 									decryptToFloat(bufferRead + 26),				/* voltage */
	// 									(float)decryptToInt64(bufferRead + 62),			/* activeEnergy */
	// 									NAN, 											/* reactiveEnergy */
	// 									decryptToFloat(bufferRead + 60),				/* activePower */
	// 									NAN	 											/* reactivePower */
	// 	);
	// 	break;
	// case MT_P:
	// 	sprintf(buffRow, "%d;%d;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f\n",
	// 									terminalID,
	// 									mesurementTime,
	// 									(float)*(bufferRead + 32),						/* current */
	// 									(float)*(bufferRead + 21),						/* current1 */
	// 									(float)*(bufferRead + 22),						/* current2 */
	// 									(float)*(bufferRead + 23),						/* current3 */
	// 									(float)*(bufferRead + 8),						/* voltage1 */
	// 									(float)*(bufferRead + 9),						/* voltage2 */
	// 									(float)*(bufferRead + 10),						/* voltage3 */
	// 									(float)*(bufferRead + 11),						/* voltage */
	// 									(float)decrypt_MT_P_ToInt64(bufferRead),	 	/* activeEnergy */
	// 									(float)decrypt_MT_P_ToInt64(bufferRead + 4), 	/* reactiveEnergy */
	// 									(float)*(bufferRead + 42),						/* activePower */
	// 									(float)*(bufferRead + 44)		 				/* reactivePower */
	// 	);
	// 	break;
	// case A9MEM15xx:
	// 	sprintf(buffRow, "%d;%d;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f\n",
	// 									terminalID,
	// 									mesurementTime,
	// 									NAN,											/* current */
	// 									decryptToFloat(bufferRead),						/* current1 */
	// 									decryptToFloat(bufferRead + 2),					/* current2 */
	// 									decryptToFloat(bufferRead + 4),					/* current3 */
	// 									decryptToFloat(bufferRead + 28),				/* voltage1 */
	// 									decryptToFloat(bufferRead + 30),				/* voltage2 */
	// 									decryptToFloat(bufferRead + 32),				/* voltage3 */
	// 									decryptToFloat(bufferRead + 20),				/* voltage */
	// 									(float)decryptToInt64(bufferRead + 44),			/* activeEnergy */
	// 									NAN, 											/* reactiveEnergy */
	// 									decryptToFloat(bufferRead + 34),				/* activePower */
	// 									decryptToFloat(bufferRead + 42)			 		/* reactivePower */
	// 	);
	// 	break;
	// 	default:
	// 		return;
	// }

	// strcat(bufferFile, buffRow);
} // end of writeMeasToCsv