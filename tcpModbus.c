
#include "tcpModbus.h"

void modbusTcp_setDebug(uint8_t debug)
{
    modbus_set_debug(ctx, debug);
}

void modbusTcp_setIpAddress(const char * _ipAddress, const char * _port)
{
    ipAddress = _ipAddress;
    port = _port;
}

int modbusTcp_connect(void)
{
	ctx = modbus_new_tcp(ipAddress,atoi(port));

	if (ctx == NULL) {
			fprintf(stderr, "\r\nUnable to allocate libmodbus context\r\n");
			return -1;
	}
	modbus_set_error_recovery(ctx, (modbus_error_recovery_mode)(MODBUS_ERROR_RECOVERY_LINK | MODBUS_ERROR_RECOVERY_PROTOCOL));

	modbus_set_response_timeout(ctx, response_timeout.tv_sec, response_timeout.tv_usec);
	if (modbus_connect(ctx) == -1) {
			fprintf(stderr, "\r\nConnection failed: %s\r\n", modbus_strerror(errno));
			modbusTcp_cleanup();
			return -1;
	}

	return 0;
}

void modbusTcp_setResponseTimeout( uint32_t sec, uint32_t usec)
{
	response_timeout.tv_sec = sec;
	response_timeout.tv_usec = usec;
}


void modbusTcp_close(void)
{
    modbus_close(ctx);
}

void modbusTcp_cleanup(void)
{
	if(ctx != NULL) {
		modbus_free(ctx);
		ctx = NULL;
	}
}

int modbusTcp_readInputRegisters(uint16_t slave, uint16_t regAddress, uint16_t regsNbr, uint16_t *buffer)
{
	if (modbusTcp_connect() == -1)
			return -1;

    modbus_set_slave(ctx, slave);
	rc = modbus_read_input_registers(ctx, regAddress, regsNbr, buffer);
	modbusTcp_close();
	modbusTcp_cleanup();

	if (rc != regsNbr) {
//		fprintf(stderr, "read input register failed: %s\r\n", modbus_strerror(errno));
		return -1;
	}

    return 0;
}

int modbusTcp_readHoldingRegisters(uint16_t slave, uint16_t regAddress, uint16_t regsNbr, uint16_t *buffer)
{
	if (modbusTcp_connect() == -1)
			return -1;

    modbus_set_slave(ctx, slave);
	rc = modbus_read_registers(ctx, regAddress, regsNbr, buffer);
	modbusTcp_close();
	modbusTcp_cleanup();

	if (rc != regsNbr) {
//		fprintf(stderr, "read input register failed: %s\r\n", modbus_strerror(errno));
		return -1;
	}

    return 0;
}

