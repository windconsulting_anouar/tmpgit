#include "timeZone.h"
//#ifdef UNIT_TEST_UTC_TIME
#include <stdio.h>
#include <string.h>
//#endif
/**
 * Method used to return the UTC time and the timeZone difference between local time and utc time
 * @param rawtime the time in second to get in utc
 * @param *timeZone the variable used to return the timezone difference
 * @return the utc time in a string
 */
char* getUTCTime(time_t rawTime,int16_t * timeZone){
	struct tm utc_tm = *gmtime(&rawTime);
	struct tm local_tm = *localtime(&rawTime);
	time_t utc=mktime(&utc_tm);// convert GMT tm to GMT time_t
	time_t local = mktime(&local_tm);
	int8_t dayLightSaving = local_tm.tm_isdst>0?local_tm.tm_isdst:0; //see http://www.cplusplus.com/reference/ctime/tm/
	*timeZone=(int16_t)((local-utc)/60+dayLightSaving*60); //difference in minute adding the daylight saving
//	printf("local %s\r\n",asctime(&local_tm));
//	printf("gmt %s\r\n",asctime(&utc_tm));
//	printf("Timezonediff = %d\r\n",*timeZone);
	//rawTime -= *timeZone*60;
	return asctime(gmtime(&rawTime));
}

char* getUTCTimeOnly(time_t rawTime){
	int16_t timeZone;
	return getUTCTime(rawTime,&timeZone);
}


//char* getUTCTimeOnly(time_t rawTime){
//	int16_t timeZone = 0;
//	struct tm utc_tm = *gmtime(&rawTime);
//	struct tm local_tm = *localtime(&rawTime);
//	time_t utc=mktime(&utc_tm);// convert GMT tm to GMT time_t
//	time_t local = mktime(&local_tm);
//	int8_t dayLightSaving = local_tm.tm_isdst>0?local_tm.tm_isdst:0; //see http://www.cplusplus.com/reference/ctime/tm/
//	timeZone=(int16_t)((local-utc)/60+dayLightSaving*60); //difference in hours adding the daylight saving
//	rawTime -= timeZone*60;
//	return asctime(localtime(&rawTime));
//}

#ifdef UNIT_TEST_UTC_TIME
//unit pour tester le changement d'heure
void unitTestGetUTCTime(void) {
	time_t rawTime;
	int16_t timeZone;
	char *strTime;
	rawTime = (time_t) 1490489940; //2017/03/26 00:59:00 GMT
	strTime=getUTCTime(rawTime, &timeZone);//
	strTime[strlen(strTime) - 1] = 0;
	printf("raw time= %d  strTime= [%s (utc)] timeZone= %.2f hour \r\n",(int)rawTime,strTime,timeZone/60.0);
	if ((timeZone / 60) != 1) {
		printf("Test failed 1 :( \r\n");
		while (1)
		;
	}
	rawTime = (time_t) 1490490060; //2017/03/26 01:01:00 GMT
	strTime=getUTCTime(rawTime, &timeZone);//
	strTime[strlen(strTime) - 1] = 0;
	printf("raw time= %d  strTime= [%s (utc)] timeZone= %.2f hour \r\n",(int)rawTime,strTime,timeZone/60.0);
	if ((timeZone / 60) != 2) {
		printf("Test failed 2 :( \r\n");
		while (1)
		;
	}
	rawTime = (time_t) 1509238740; //2017/10/29 00:59:00 GMT
	strTime=getUTCTime(rawTime, &timeZone);//
	strTime[strlen(strTime) - 1] = 0;
	printf("raw time= %d  strTime= [%s (utc)] timeZone= %.2f hour \r\n",(int)rawTime,strTime,timeZone/60.0);
	if ((timeZone / 60) != 2) {
		printf("Test failed 3 :( \r\n");
		while (1)
		;
	}
	rawTime = (time_t) 1509238860; //2017/10/29 01:01:00 GMT
	strTime=getUTCTime(rawTime, &timeZone);//
	strTime[strlen(strTime) - 1] = 0;
	printf("raw time= %d  strTime= [%s (utc)] timeZone= %.2f hour \r\n",(int)rawTime,strTime,timeZone/60.0);
	if ((timeZone / 60) != 1) {
		printf("Test failed 4 :( \r\n");
		while (1)
		;
	}
	rawTime = (time_t) 1521939540; //2018/03/25 00:59:00 GMT
	strTime=getUTCTime(rawTime, &timeZone);//
	strTime[strlen(strTime) - 1] = 0;
	printf("raw time= %d  strTime= [%s (utc)] timeZone= %.2f hour \r\n",(int)rawTime,strTime,timeZone/60.0);
	if ((timeZone / 60) != 1) {
		printf("Test failed 5 :( \r\n");
		while (1)
		;
	}
	rawTime = (time_t) 1521939660; //2018/03/25 01:59:00 GMT
	strTime=getUTCTime(rawTime, &timeZone);//
	strTime[strlen(strTime) - 1] = 0;
	printf("raw time= %d  strTime= [%s (utc)] timeZone= %.2f hour \r\n",(int)rawTime,strTime,timeZone/60.0);
	if ((timeZone / 60) != 2) {
		printf("Test failed :( \r\n");
		while (1)
		;
	}
	printf("Test succeed :) \r\n");


	rawTime = (time_t) 1521939000; //2018/03/25 01:59:00 GMT
	strTime=getUTCTime(rawTime, &timeZone);//
	strTime[strlen(strTime) - 1] = 0;
	printf("raw time= %d  strTime= [%s (utc)] timeZone= %.2f hour \r\n",(int)rawTime,strTime,timeZone/60.0);
	rawTime = (time_t) 1521939600; //2018/03/25 01:59:00 GMT
	strTime=getUTCTime(rawTime, &timeZone);//
	strTime[strlen(strTime) - 1] = 0;
	printf("raw time= %d  strTime= [%s (utc)] timeZone= %.2f hour \r\n",(int)rawTime,strTime,timeZone/60.0);
//	rawTime = (time_t) 1521939600; //2018/03/25 01:59:00 GMT
//	strTime=getUTCTime(rawTime, &timeZone);//
//	strTime[strlen(strTime) - 1] = 0;
//	printf("raw time= %d  strTime= [%s (utc)] timeZone= %.2f hour \r\n",(int)rawTime,strTime,timeZone/60.0);
}
#endif

