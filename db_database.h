

#ifndef __DATABASE__
#define __DATABASE__

#include <stdint.h>
#include <stdbool.h>
#include <time.h>


bool databaseConnect(const char *host, const char *port, const char *dbname,
                     const char *username, const char *password);

bool databaseDisconnect(void);

bool databaseInsertStandard2 (uint32_t terminal_id, time_t datetime, double ip, double iph1, double iph2, double iph3, double up, double uph1, double uph2, double uph3, double e, double er, double pa, double pr, bool storedOnserver);

#endif //__DATABASE__

